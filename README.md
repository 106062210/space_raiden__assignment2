# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|N|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : At first, there will only be a row of enemies. If the player scores during gameplay, the enemies will come in two rows or three rows.
2. Animations : Adding animation of tilting to player when player moves left or right, the plane has a animation of coming back from tilt too.
3. Particle Systems : 
4. Sound effects : Adding bullet sound effect and playing it on bullet shot. Adding background music and play it when game is created.

# Bonus Functions Description : 
1. Unique bullet : Every 2000 score the player gets, the game randomly upgrades one of three systems of Raiden's attack system: Bullet, Power, or Speed.
