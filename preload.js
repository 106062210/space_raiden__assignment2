var preloadstate = {preload:preload}

function preload(){
    game.load.image('bg','assets/bg.png');
    game.load.image('bullet0','assets/bullet0.png');
    game.load.image('bullet1','assets/bullet1.png');
    game.load.image('bullet2','assets/bullet2.png');
    game.load.image('redenemy','assets/enemyred_nobg.png');
    game.load.image('blueenemy','assets/enemyblue_nobg.png');
    game.load.image('greenenemy','assets/enemygreen_nobg.png');
    game.load.image('enemybullet_blue','assets/enemybullet_blue.png');
    game.load.image('enemybullet_green','assets/enemybullet_green.png');
    game.load.image('ultimateskill','assets/ultimateskill.png');
    game.load.spritesheet('player','assets/playernobg.png',playerwidth,playerheight);
    game.load.audio('bulletaudio','assets/blaster.mp3');
    game.load.audio('music','assets/star_wars.mp3');
}
