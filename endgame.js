var endgame = {
    preload:preload,
    create:create,
    update:update
}

var T1;
var T2;
var T3;

function create(){
    bg = game.add.tileSprite(0,0,game.width,game.height,'bg');
    T1 = game.add.text(game.world.centerX-100, game.world.centerY, "Game Over");
    T2 = game.add.text(game.world.centerX+100, game.world.centerY, "Score:");
    T3 = game.add.text(game.world.centerX, game.world.centerY+70, "Press Enter to replay\nPress Esc to go to menu");
}
function update(){
    bg.tilePosition.y+=1;
    drawtext();
    Choose();
}
function drawtext(){
    T1.anchor.setTo(0.5);
    T1.fontSize = 36;
    T1.fill = "#ffffff";

    T2.text = "Score: "+game.global.score;
    T2.anchor.setTo(0.5);
    T2.fontSize = 36;
    T2.fill = "#ffffff";

    T3.anchor.setTo(0.5);
    T3.fontSize = 36;
    T3.fill = "#ffffff";
    T3.align = "center";
}
function Choose(){
    if(enter.isDown){
        game.state.start('main');
    }
    if(esc.isDown){
        game.state.start('menu');
    }
}
