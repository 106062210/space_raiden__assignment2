var bg;
var faceleft;
var faceright;
var bullets0;
var bullets1;
var bullets2;
var enemybullets_blue;
var enemybullets_green;
var bulletTime = 0;
var firebutton;
var pausebutton;
var pause;
var playerwidth = 50;
var playerheight = 56;
var powerstage;
var speedstage;
var bulletstage;
var totallevel;
var init = 0;
var redposition;
var blueposition;
var greenposition;
var waittime;
var bulletaudio;
var music;
var ultimateskill;
var ultimateskillcount;
var ultimatecooldown;

var UItext1;// Lives
var UItext2;// Score
var UItext3;// Ultimate cooldown boolean
var UItext4;// Bullet Power Speed
var UItext5;// Press P to Pause
var Pausetext;

function unpause(event){
    if(game.paused == true) {
        game.paused = false;
        Pausetext.text = "";
    }
    else {
        Pausetext.text = "-paused-";
        UItext5.text = "Press P to\n unpuase";
        game.paused = true;
    }
};

var mainState = {

    preload: function() {
    },

    create: function() {

        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.global.score = 0;
        ultimatecooldown = 0;
        ultimateskillcount = 3;

        
        bg = game.add.tileSprite(0,0,game.width*4/5,game.height,'bg');
        game.physics.enable(bg, Phaser.Physics.ARCADE);

        bulletaudio = game.add.audio('bulletaudio');


        bullets0 = game.add.group();
        bullets1 = game.add.group();
        bullets2 = game.add.group();
        bullets0.enableBody = true;
        bullets1.enableBody = true;
        bullets2.enableBody = true;
        bullets0.physicsBodyType = Phaser.Physics.ARCADE;
        bullets1.physicsBodyType = Phaser.Physics.ARCADE;
        bullets2.physicsBodyType = Phaser.Physics.ARCADE;
        bullets0.createMultiple(20,'bullet0');
        bullets1.createMultiple(20,'bullet1');
        bullets2.createMultiple(20,'bullet2');
        bullets0.setAll('outOfBoundsKill',true);
        bullets0.setAll('checkWorldBounds',true);
        bullets0.setAll('anchor.x',0.5);
        bullets0.setAll('anchor.y',1);
        bullets1.setAll('outOfBoundsKill',true);
        bullets1.setAll('checkWorldBounds',true);
        bullets1.setAll('anchor.x',0.5);
        bullets1.setAll('anchor.y',1);
        bullets2.setAll('outOfBoundsKill',true);
        bullets2.setAll('checkWorldBounds',true);
        bullets2.setAll('anchor.x',0.5);
        bullets2.setAll('anchor.y',1);

        ultimateskill = game.add.group();
        ultimateskill.enableBody = true;
        ultimateskill.physicsBodyType = Phaser.Physics.ARCADE;
        ultimateskill.createMultiple(3,'ultimateskill');
        ultimateskill.setAll('outOfBoundsKill',true);
        ultimateskill.setAll('checkWorldBounds',true);
        ultimateskill.setAll('anchor.x',0);
        ultimateskill.setAll('anchor.y',0);

        enemybullets_blue = game.add.group();
        enemybullets_blue.enableBody = true;
        enemybullets_blue.physicsBodyType = Phaser.Physics.ARCADE;
        enemybullets_blue.createMultiple(20,'enemybullet_blue');
        enemybullets_blue.setAll('outOfBoundsKill',true);
        enemybullets_blue.setAll('checkWorldBounds',true);

        enemybullets_green = game.add.group();
        enemybullets_green.enableBody = true;
        enemybullets_green.physicsBodyType = Phaser.Physics.ARCADE;
        enemybullets_green.createMultiple(20,'enemybullet_blue');
        enemybullets_green.setAll('outOfBoundsKill',true);
        enemybullets_green.setAll('checkWorldBounds',true);
        
        redenemies = game.add.group();
        redenemies.enableBody = true;
        redenemies.physicsBodyType = Phaser.Physics.ARCADE;
        redenemies.createMultiple(20,'redenemy');
        redenemies.setAll('outOfBoundsKill',true);
        redenemies.setAll('checkWorldBounds',true);

        // console.log('redenemies'+redenemies.total);

        blueenemies = game.add.group();
        blueenemies.enableBody = true;
        blueenemies.physicsBodyType = Phaser.Physics.ARCADE;
        blueenemies.createMultiple(20,'blueenemy');
        blueenemies.setAll('outOfBoundsKill',true);
        blueenemies.setAll('checkWorldBounds',true);

        greenenemies = game.add.group();
        greenenemies.enableBody = true;
        greenenemies.physicsBodyType = Phaser.Physics.ARCADE;
        greenenemies.createMultiple(20,'greenenemy');
        greenenemies.setAll('outOfBoundsKill',true);
        greenenemies.setAll('checkWorldBounds',true);

        redenemies.setAll('anchor.y',1);
        blueenemies.setAll('anchor.y',1);
        greenenemies.setAll('anchor.y',1);

        firebutton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        ultimateskillbutton =  game.input.keyboard.addKey(Phaser.Keyboard.Z);
        pausebutton = game.input.keyboard.addKey(Phaser.Keyboard.P);

        game.renderer.renderSession.roundPixels = true;

        // this.tree = game.add.sprite(50,160,'tree');
        // this.tree.scale.setTo(1.5,1.5);

        this.player = game.add.sprite(game.width*2/5, 280, 'player');
        this.player.animations.add('leftmove',[0,4,3],10,false);
        this.player.animations.add('rightmove',[0,1,2],10,false);
        this.player.animations.add('leftreturn',[3,4,0],10,false);
        this.player.animations.add('rightreturn',[2,1,0],10,false);
        this.player.HP = 2;
        this.cursor = game.input.keyboard.createCursorKeys();
        game.physics.enable(this.player,Phaser.Physics.ARCADE);

        powerstage = 0;
        speedstage = 0;
        bulletstage = 0;

        UItext1 = game.add.text(game.width*4/5+20,20,"Lives: ");
        UItext2 = game.add.text(game.width*4/5+20,50,"Score: ");
        UItext3 = game.add.text(game.width*4/5+20,80,"Ult: ");
        UItext4 = game.add.text(game.width*4/5+20,140,"Bullet: Power: Speed: ");
        UItext5 = game.add.text(game.width*4/5+20,230,"Press P to\n Pause");
        UItext1.fontSize = 15;
        UItext2.fontSize = 15;
        UItext3.fontSize = 15;
        UItext4.fontSize = 15;
        UItext5.fontSize = 20;
        UItext1.fill = "#ffffff";
        UItext2.fill = "#ffffff";
        UItext3.fill = "#ffffff";
        UItext4.fill = "#ffffff";
        UItext5.fill = "#ffffff";
        UItext5.align = "center";

        Pausetext = game.add.text(game.world.CenterX,game.world.CenterY,"");
        Pausetext.fill="#fffffff";
        Pausetext.fontSize=20;
        pausebutton.onDown.add(unpause,self);
    },

    update: function() {
        
        game.physics.arcade.overlap(bullets0,redenemies,this.hitred,null,this);
        game.physics.arcade.overlap(bullets0,blueenemies,this.hitblue,null,this);
        game.physics.arcade.overlap(bullets0,greenenemies,this.hitgreen,null,this);
        game.physics.arcade.overlap(bullets1,redenemies,this.hitred,null,this);
        game.physics.arcade.overlap(bullets1,blueenemies,this.hitblue,null,this);
        game.physics.arcade.overlap(bullets1,greenenemies,this.hitgreen,null,this);
        game.physics.arcade.overlap(bullets2,redenemies,this.hitred,null,this);
        game.physics.arcade.overlap(bullets2,blueenemies,this.hitblue,null,this);
        game.physics.arcade.overlap(bullets2,greenenemies,this.hitgreen,null,this);
        game.physics.arcade.overlap(ultimateskill,redenemies,this.ultimatehit,null,this);
        game.physics.arcade.overlap(ultimateskill,blueenemies,this.ultimatehit,null,this);
        game.physics.arcade.overlap(ultimateskill,greenenemies,this.ultimatehit,null,this);
        game.physics.arcade.overlap(this.player,redenemies,this.Playercollide,null,this);
        game.physics.arcade.overlap(this.player,blueenemies,this.Playercollide,null,this);
        game.physics.arcade.overlap(this.player,greenenemies,this.Playercollide,null,this);

        this.writetext();
        this.movePlayer();

        if(firebutton.isDown){
            this.Playershoot();
        }
        if(ultimateskillbutton.isDown){
            this.fireultimate();
        }
        bg.tilePosition.y += 1;
        if(!redenemies.total&&!blueenemies.total&&!greenenemies.total&&ultimateskill.getFirstAlive()==null){
            // if(game.global.score>10000){this.createboss();}
            // else{
                this.createenemy();
                init = 0;
            // }
        }
        else if(init==0){
            if(!this.checkenemyinited()){
                this.initenemy();waittime = game.time.now + 1000
            }
            else{init = 1};
        }
        else{
            if(game.time.now<waittime){this.enemywait();}
            else{this.enemymove();}
        }
        totallevel = powerstage+speedstage+bulletstage;
        if(totallevel<Math.floor(game.global.score/2000)&&total!=6){this.Playerupgrade();}        
    },
    writetext: function(){
        var ultavailable;
        if((game.time.now>ultimatecooldown)&&ultimateskillcount) ultavailable = true;
        else ultavailable = false;
        UItext1.text = "Lives: "+ (this.player.HP+1);
        UItext2.text = "Score: "+ game.global.score;
        UItext3.text = "Ult: "+ ultimateskillcount+"\nAvailable: "+ ultavailable;
        UItext4.text = "Bullet: "+bulletstage+"\nPower: "+powerstage+"\nSpeed: "+speedstage;
        if(game.paused){UItext5.text = "Game Paused\n Press P to\n Continue";}
        else {UItext5.text = "Press P to\n Pause";}
    },
    Playerupgrade: function(){
        rdn = Math.floor(Math.random()*3);
        console.log(rdn);
        if(totallevel==0){
            if(rdn == 0){powerstage =1;}
            else if(rdn == 1){speedstage = 1;}
            else if(rdn == 2){bulletstage = 1;}
        }
        else if(totallevel==1){
            if(rdn == 0){powerstage +=1; return}
            else if(rdn == 1){speedstage +=1; return}
            else if(rdn == 2){bulletstage +=1; return}
        }
        else if(totallevel==2){
            if(rdn == 0){
                if(powerstage!=2)powerstage +=1;
                else speedstage+=1;
            }
            else if(rdn == 1){
                if(speedstage!=2)speedstage +=1;
                else bulletstage+=1;
            }
            else if(rdn == 2){
                if(bulletstage!=2)bulletstage +=1;
                else powerstage+=1;
            }
        }
        else if(totallevel==3){
            if(rdn == 0){
                if(powerstage!=2)powerstage +=1;
                else speedstage+=1;
            }
            else if(rdn == 1){
                if(speedstage!=2)speedstage +=1;
                else bulletstage+=1;
            }
            else if(rdn == 2){
                if(bulletstage!=2)bulletstage +=1;
                else powerstage+=1;
            }
        }
        else if(totallevel==4){
            if(rdn == 0){
                if(powerstage!=2)powerstage +=1;
                else if(speedstage!=2) speedstage+=1;
                else bulletstage+=1;
            }
            else if(rdn == 1){
                if(speedstage!=2)speedstage +=1;
                else if(bulletstage!=2) bulletstage+=1;
                else powerstage+=1;
            }
            else if(rdn == 2){
                if(bulletstage!=2)bulletstage +=1;
                else if(powerstage!=2) powerstage+=1;
                else speedstage+=1;
            }
        }
        else if(totallevel==5){
            powerstage = 2;
            speedstage = 2;
            bulletstage = 2;
        }
    },

    movePlayer: function() {
        if(this.cursor.left.isDown&&this.player.x>0){
            this.player.x-=5;
            if(!faceleft){
                this.player.animations.play('leftmove');
                faceleft = 1;
            }
        }
        else if(this.cursor.right.isDown&&this.player.x<game.width*4/5-playerwidth){
            this.player.x+=5;
            if(!faceright){
                this.player.animations.play('rightmove');
                faceright = 1;
            }
        }
        else if(this.cursor.down.isDown&&this.player.y<game.height-playerheight){
            this.player.y+=5;
        }
        else if(this.cursor.up.isDown&&this.player.y>0){
            this.player.y-=5;
        }
        else if(!this.cursor.right.isDown&&!this.cursor.left.isDown&&faceright){
            this.player.animations.play('rightreturn');
            faceright = 0;
        }
        else if(!this.cursor.left.isDown&&!this.cursor.right.isDown&&faceleft){
            this.player.animations.play('leftreturn');
            faceleft = 0;
        }
    },
    Playershoot: function(){
        if(game.time.now >bulletTime){
            if(bulletstage>=0){
                if(powerstage==0){bullet = bullets0.getFirstExists(false);}
                else if(powerstage==1){bullet = bullets1.getFirstExists(false);}
                else if(powerstage==2){bullet = bullets2.getFirstExists(false);}
            }
            if(bulletstage>=1){
                bullet.reset(0,0);
                if(powerstage==0){bullet1 = bullets0.getFirstExists(false);}
                else if(powerstage==1){bullet1 = bullets1.getFirstExists(false);}
                else if(powerstage==2){bullet1 = bullets2.getFirstExists(false);}
            }
            if(bulletstage>=2){
                bullet1.reset(0,0);
                if(powerstage==0){bullet2 = bullets0.getFirstExists(false);}
                else if(powerstage==1){bullet2 = bullets1.getFirstExists(false);}
                else if(powerstage==2){bullet2 = bullets2.getFirstExists(false);}
            }
            if(bulletstage==2&&bullet&&bullet1&&bullet2){
                bullet.reset(this.player.x+playerwidth/2,this.player.y+playerheight/2);
                bullet1.reset(this.player.x+playerwidth/2+15,this.player.y+playerheight/2);
                bullet2.reset(this.player.x+playerwidth/2-15,this.player.y+playerheight/2);
                if(speedstage==0){
                    bullet.body.velocity.y = - 400;
                    bullet1.body.velocity.y = - 400;
                    bullet2.body.velocity.y = - 400;
                    bulletTime = game.time.now + 500;
                }
                if(speedstage==1){
                    bullet.body.velocity.y = - 500;
                    bullet1.body.velocity.y = - 500;
                    bullet2.body.velocity.y = - 500;
                    bulletTime = game.time.now + 300;
                }
                if(speedstage==2){
                    bullet.body.velocity.y = - 600;
                    bullet1.body.velocity.y = - 600;
                    bullet2.body.velocity.y = - 600;
                    bulletTime = game.time.now + 100;
                }
            }
            if(bulletstage==1&&bullet&&bullet1){
                bullet.reset(this.player.x+playerwidth/2+20,this.player.y+playerheight/2);
                bullet1.reset(this.player.x+playerwidth/2-20,this.player.y+playerheight/2);
                if(speedstage==0){
                    bullet.body.velocity.y = - 400;
                    bullet1.body.velocity.y = - 400;
                    bulletTime = game.time.now + 500;
                }
                if(speedstage==1){
                    bullet.body.velocity.y = - 500;
                    bullet1.body.velocity.y = - 500;
                    bulletTime = game.time.now + 300;
                }
                if(speedstage==2){
                    bullet.body.velocity.y = - 600;
                    bullet1.body.velocity.y = - 600;
                    bulletTime = game.time.now + 100;
                }                
            }
            if(bulletstage==0&&bullet){
                bullet.reset(this.player.x+playerwidth/2,this.player.y+playerheight/2);
                if(speedstage==0){
                    bullet.body.velocity.y = - 400;
                    bulletTime = game.time.now + 500;
                }
                if(speedstage==1){
                    bullet.body.velocity.y = - 500;
                    bulletTime = game.time.now + 300;
                }
                if(speedstage==2){
                    bullet.body.velocity.y = - 600;
                    bulletTime = game.time.now + 100;
                }
            }
            bulletaudio.play();
        }
    },
    fireultimate: function(){
        if(game.time.now >ultimatecooldown&&ultimateskillcount){
            console.log("ultimateskill fired");
            ult = ultimateskill.getFirstExists(false);
            ult.reset(0,game.height-1);
            ult.body.velocity.y -= 1000;
            ultimateskillcount-=1;
            ultimatecooldown = game.time.now +=10000;
        }
    },
    Playerdie: function(){
        if(!this.player.HP){game.state.start('endgame');}
        else{
            this.player.HP-=1
            this.player.x = game.width*2/5;
            this.player.y = 280;
            
        }
    },
    Playercollide: function(player,enemy){
        this.Playerdie();
        this.enemydie(enemy);
    },
    createenemy: function(){
        if(game.global.score<200){
            //create 10 red
            for(i=0;i<7;i++){
                redenemy = redenemies.getFirstExists(false);
                redenemy.reset(60+i*60,0);
                redenemy.HP = 3;
                redenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                redenemy.score = 100;
            }
            redposition = 50;
        }
        else if(game.global.score<1000){
            //create 10 red 10 blue
            for(i=0;i<7;i++){
                redenemy = redenemies.getFirstExists(false);
                redenemy.reset(60+i*60,0);
                redenemy.HP = 3;
                redenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                blueenemy = blueenemies.getFirstExists(false);
                blueenemy.reset(60+i*60,0);
                blueenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                redenemy.score = 100;
                blueenemy.score = 125;
            }
            redposition = 100;
            blueposition = 50;
        }
        else{
            //create 10 red 10 blue 10 green
            for(i=0;i<7;i++){
                redenemy = redenemies.getFirstExists(false);
                redenemy.reset(60+i*60,0);
                redenemy.HP = 3;
                redenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                blueenemy = blueenemies.getFirstExists(false);
                blueenemy.reset(60+i*60,0);
                blueenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                greenenemy = greenenemies.getFirstExists(false);
                greenenemy.reset(60+i*60,0);
                greenenemy.HP = 2;
                greenenemy.waittime = game.time.now+Math.floor(Math.random()*200);
                redenemy.score = 100;
                blueenemy.score = 125;
                greenenemy.score = 150;
            }
            redposition = 150;
            blueposition = 100;
            greenposition = 50;
        }
    },
    initenemy:function(){
        redenemies.setAll('body.velocity.y',redposition*2);
        blueenemies.setAll('body.velocity.y',blueposition*2);
        greenenemies.setAll('body.velocity.y',greenposition*2);
    },
    checkenemyinited:function(){
        if(redenemies.getFirstAlive()!=null) return redenemies.getFirstAlive().y>redposition;
        else if(blueenemies.getFirstAlive()!=null) return blueenemies.getFirstAlive().y>blueposition;
        else if(greenenemies.getFirstAlive()!=null) return greenenemies.getFirstAlive().y>greenposition;
        else return false;
    },
    enemywait: function(){
        redenemies.setAll('body.velocity.y',0);
        blueenemies.setAll('body.velocity.y',0);
        greenenemies.setAll('body.velocity.y',0);
    },
    enemymove: function(){
        //red enemy go forward
        redenemies.setAll('body.velocity.y',300);
        //blue enemy fire blue bullet
        blueenemies.setAll('body.velocity.y',600);
        //green enemy fire green bullet
        greenenemies.setAll('body.velocity.y',500);
    },
    hitred: function(bullet,enemy){
        bullet.kill();
        if(enemy.HP==1){
            this.enemydie(enemy);
        }
        else{enemy.HP-=1;}
    },
    hitblue: function(bullet,enemy){
        bullet.kill();
        this.enemydie(enemy);
    },
    hitgreen: function(bullet,enemy){
        bullet.kill();
        if(enemy.HP==1){
            this.enemydie(enemy);
        }
        else{enemy.HP-=1;}
    },
    enemydie: function(enemy){
        enemy.kill();
        game.global.score += enemy.score;
    },
    ultimatehit: function(ultimate,enemy){
        this.enemydie(enemy);
    },
    
    createboss: function(){

    }
};

